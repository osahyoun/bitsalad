require 'uri'

class TwitterSearchBuilder
  attr_reader :params

  def initialize(params)
    @params = params
  end

  def build
    #URI.encode(
      [all, exact, any, none, hash].
      compact.join(' ')
    #)
  end

  def hash
    params[:hash].
      strip.
      gsub(/#/, '').
      split(/\s+/).
      map{|a| "##{a}"}.
      join(' ') unless params[:hash].nil?
  end

  def all
    params[:all].
      strip.
      gsub(/\s+/, ' ') unless params[:all].nil?
  end

  def any
    params[:any].
      strip.
      gsub(/\s+/, ' ').
      split(/\s/).
      join(' OR ') unless params[:any].nil?
  end

  def none
    params[:none].
      strip.
      split(/\s+/).
      map{|a| "-#{a}"}.
      join(' ') unless params[:none].nil?
  end

  def exact
    if params[:exact]
      q = params[:exact].
        strip.
        gsub(/\s+/, ' ')

      %{"#{q}"} unless q.blank?
    end
  end
end
