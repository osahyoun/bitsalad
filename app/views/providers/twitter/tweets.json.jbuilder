json.array! @tweets do |tweet|
  json.id tweet.id
  json.created_at tweet.created_at
  json.text tweet.text
  json.retweet_count tweet.retweet_count
  json.favorite_count tweet.favorite_count
  json.in_reply_to_status_id tweet.in_reply_to_status_id
  json.in_reply_to_user_id tweet.in_reply_to_user_id
  json.in_reply_to_screen_name tweet.in_reply_to_screen_name

  if tweet.media.any?
    json.media tweet.media do |media|
      json.media_url media.media_url.to_s
      json.media_thumbnail_url media.media_url.to_s + ':thumb'
    end
  end

  json.urls tweet.urls

  json.user tweet.user do |user|
    json.screen_name user.screen_name
    json.name user.name
    json.location user.location
    json.url user.url
    json.followers_count user.followers_count
    json.favorites_count user.favorites_count
  end

  if tweet.place?
    json.place tweet.place.name
  elsif tweet.geo?

  end
end
