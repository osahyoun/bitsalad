json.array! @media do |media|
  json.tags media.tags
  json.type media.type
  json.filter media.filter
  json.likes_count media.likes.count
  json.caption (media.caption ? media.caption.text : '')
  json.image_low_resolution  media.images.low_resolution
  json.image_thumbnail media.images.thumbnail
  json.image_standard_resolution media.images.standard_resolution
end
