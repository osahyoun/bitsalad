$ ->
  ## Feed tabs
  $('.resources a').on 'click', (e) ->
    e.preventDefault()
    $el = $(this)
    section = $el.data('section')
    tab = $el.data('tab')

    $("ul.menu-#{section} li").removeClass('active')
    $el.parent().addClass('active')
    $(".feed-section.#{section}").hide()
    $(".feed-section.#{section}-#{tab}").show()
    false

  ## Select URI when clicking on link
  $('.feeds-table').on 'click', 'input.service-uri', (e) ->
    $(@).select()


  ## Deleting feed
  #
  $('.feeds-table').on 'ajax:success', 'a.delete-feed', (e) ->
    $(@).parents('tr').remove()

  $('form.build-feeds-form').on 'ajax:complete', ->
    $(@).find('.btn').removeClass('disabled')

  $('form.build-feeds-form').on 'ajax:before', ->
    $('.errors').text('')
    $(@).find('.btn').addClass('disabled')

  $('form.build-feeds-form').on 'ajax:success', (e, data, status, xhr) ->
    if data.status == 'errors'
      $(@).find('.errors').text(data.errors[0])
    else
      $('.feeds-table').html data.data
      $(@).find('input.text-field').val('')

  $('form.create-feed').on 'ajax:error', ->
    #$(@).parents('.feed').find('.feeds-table').html data.data
    #$(@).find('input.text-field').val('')
    console.log "ERROR"
