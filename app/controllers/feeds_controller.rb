class FeedsController < ApplicationController
  before_action :create_feed, only: [:create]

  def show
    find_feed

    @feed = current_user.feeds.find params[:id]
  end

  def index
    @feeds = current_user.feeds
  end

  def destroy
    find_feed

    @feed.destroy
    puts 'Destroy feed'

    respond_to do |format|
      format.json { render(json: @feed, status: 200) }
    end
  end

  def create
    if @feed.errors.empty?
      success_as_json
    else
      errors_as_json
    end
  end

  private

  def find_feed
    @feed = current_user.feeds.find params[:id]
  end

  def response_to_json
    respond_to do |format|
      format.json do
        type = self.class.to_s.deconstantize.downcase
        render(json: { data: render_to_string(partial: '/feeds/table', locals: {type: type}, formats: [:html]) }, status: 200)
      end
    end
  end
  alias_method :success_as_json, :response_to_json

  def errors_as_json
    respond_to do |format|
      format.json do
        render(json: { errors: @feed.errors.values, status: :errors })
      end
    end
  end

  def create_feed
  end
end
