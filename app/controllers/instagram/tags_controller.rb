class Instagram::TagsController < ::FeedsController
  def new
  end

  def create
    @feed = Instagram::Tag.create(user: current_user, tag: params[:instagram_tag][:tag])

    if @feed.valid?
      success_as_json
    else
      errors_as_json
    end
  end
end

