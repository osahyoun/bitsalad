class Instagram::UsersController < ::FeedsController
  def show
    user = FeedManagement::Instagram.find_user(provider.token, params[:id])
    render json: user
  end

  def new
    @user = Instagram::User.new
  end

  def create
    @feed = Instagram::User.create(user: current_user, handle: params[:instagram_user][:handle])
    if @feed.valid?
      success_as_json
    else
      errors_as_json
    end
  end
end
