class ProvidersController < ApplicationController
  skip_before_filter :signed_in?, only: [:create]

  def create
    if env["omniauth.params"]["to"] == 'signin'
      user = NewUserManager.new(request.env["omniauth.auth"]).create
      session[:user_id] = user.id
    else
      ProviderStore.create(current_user, request.env["omniauth.auth"])
    end
    redirect_to root_path
  end

  def destroy
    provider = current_user.providers.find(params[:id])
    provider.destroy if provider

    redirect_to root_path
  end
end
