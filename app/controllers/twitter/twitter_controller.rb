class Twitter::FeedsController < ApplicationController
  def new
    render json: {name: 'omar'}
  end

  def create
    @timeline = Twitter::Timeline.create(user: current_user, screen_name: params[:twitter_timeline][:screen_name])
    unless @timeline.valid?
      render :new
    else
      redirect_to root_path
    end
  end
end

