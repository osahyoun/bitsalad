class Twitter::SearchesController < ::FeedsController
  def new
    @search = Twitter::Search.new
  end

  def create
    q = TwitterSearchBuilder.new(params).build

    @feed = Twitter::Search.create(user: current_user, q: q)

    if @feed.valid?
      success_as_json
    else
      errors_as_json
    end
  end
end

