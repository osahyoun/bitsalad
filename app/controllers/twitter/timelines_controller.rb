class Twitter::TimelinesController < ::FeedsController
  def new
  end

  def create
    @feed = Twitter::Timeline.create(user: current_user, screen_name: params[:twitter_timeline][:screen_name])

    if @feed.valid?
      success_as_json
    else
      errors_as_json
    end
  end
end
