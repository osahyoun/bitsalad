class HomeController < ApplicationController
  skip_before_filter :signed_in?

  def index
    #redirect_to new_user_registration_path unless current_user
    if current_user
      render 'index'
    else
      render 'home'
    end
  end

  def home

  end
end

