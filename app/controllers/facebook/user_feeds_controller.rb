class Facebook::UserFeedsController < ::FeedsController

  private

  def create_feed
    @feed = Facebook::UserFeed.create(user: current_user)
  end
end
