class Facebook::PagesController < ::FeedsController


  private

  def create_feed
    @feed = Facebook::Page.create(user: current_user, page: params[:facebook_page][:page])
  end
end
