module Feeds
  module Instagram
    class RecentMediaController < ApplicationController
      def create
        id = FeedManagement::Instagram.get_id_from_handle(provider.token, handle)
        render text: id
      end

      private

      def provider
        @provider ||= Provider.instagram_for_user(current_user)
      end

      def handle
        params[:handle]
      end
    end
  end
end

