class Feeds::InstagramFeedsController < ApplicationController
  def create
    get_provider
    make_new_feed

    if @feed.save
      render json: @feed
    end
  end

  private

  def get_instagram_user_id
    FeedManagement::Instagram.get_id_from_handle(@provider.token, safe_params[:handle])
  end

  def make_new_feed
    id = get_instagram_user_id
    @feed = Feed.new(user: current_user, provider: @provider, meta: {user_id: id})
  end

  def get_provider
    @provider = Provider.instagram_for_user(current_user)
  end

  def find_feed
    @feed = current_user.feeds.find params[:id]
  end

  def safe_params
    params.require(:feed).permit(:query, :handle)
  end
end

