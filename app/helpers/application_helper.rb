module ApplicationHelper
  def url_for_api(id)
    if Rails.env.development?
      protocol = 'http://'
      port = ':3002'
    else
      protocol = 'http://'
      port = ''
    end

    host = 'api.bitsalad.co'

    "#{protocol}#{host}#{port}/v1/feeds/#{id}"
  end
end
