module Presenters
  module Feeds
    class Facebook < BasicFeed
      def query
        case feed.resource
        when 'user/feed'
          "Feed posts for #{feed.meta['name']}"
        when 'page'
          "Facebook page <a target='_blank' href='#{feed.meta['url']}'>#{feed.meta['name']}</a>".html_safe
        end
      end
    end
  end
end
