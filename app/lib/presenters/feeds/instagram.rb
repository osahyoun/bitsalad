module Presenters
  module Feeds
    class Instagram < BasicFeed
      def query
        case feed.resource
        when 'users/media/recent'
          "Media for user #{feed.meta['handle']}"
        when 'tags/media/recent'
          "Media for tag #{feed.meta['tag']}"
        end
      end
    end
  end
end

