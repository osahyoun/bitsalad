module Presenters
  module Feeds
    class BasicFeed
      delegate :fetched_at, :id, to: :feed

      attr_reader :feed

      def initialize(feed)
        @feed = feed
      end
    end
  end
end

