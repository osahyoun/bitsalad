module Presenters
  module Feeds
    class Twitter < BasicFeed
      def query
        case feed.resource
        when 'statuses/user_timeline'
          "Timeline: <strong>@#{feed.meta["screen_name"]}</strong>".html_safe
        else
          "Search: <strong>#{URI.decode(feed.meta['q'])}</strong>".html_safe
        end
      end
    end
  end
end
