module ApiWrappers
  class Twitter
    def initialize(token, secret)
      @token = token
      @secret = secret
    end

    def search(q)
      client.search(q)
    end

    def statuses_timeline(screen_name)
      @timeline = client.user_timeline(screen_name: screen_name)
    end

    private

    def client
      @client ||= ::Twitter::REST::Client.new do |config|
        config.consumer_key        = ENV['TWITTER_KEY']
        config.consumer_secret     = ENV['TWITTER_SECRET']
        config.access_token        = @token
        config.access_token_secret = @secret
      end
    end
  end
end


#client ||= ::Twitter::REST::Client.new do |config|
  #config.consumer_key        = ENV['TWITTER_KEY']
  #config.consumer_secret     = ENV['TWITTER_SECRET']
  #config.access_token        = Provider.last.token
  #config.access_token_secret = Provider.last.secret
#end

