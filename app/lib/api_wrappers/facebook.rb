module ApiWrappers
  class Facebook
    def initialize(token)
      @token = token
    end

    def profile
      client.get_object 'me'
    end

    private

    def client
      @client ||= Koala::Facebook::API.new(@token)
    end
  end
end
