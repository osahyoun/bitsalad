module ApiWrappers
  class Instagram
    def initialize(token)
      @token = token
    end

    def user_search(username)
      client.user_search(username)
    end

    def tag_recent_media(tag)
      client.tag_recent_media(tag)
    end

    def user_recent_media(id)
      client.user_recent_media(id)
    end

    private

    def client
      @client ||= ::Instagram.client access_token: @token
    end
  end
end

