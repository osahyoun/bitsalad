class Twitter::Search < Twitter::Model
  attr_accessor :q
  validates :q, presence: true

  RESOURCE = 'search'

  def meta
    { q: URI.escape(q) }
  end
end
