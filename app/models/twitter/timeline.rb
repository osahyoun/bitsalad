class Twitter::Timeline < Twitter::Model
  attr_accessor :screen_name

  validates :screen_name, presence: true
  validate  :user_exists

  before_save :trim

  RESOURCE = 'statuses/user_timeline'

  def meta
    { screen_name: screen_name }
  end

  def user_exists
    begin
      client.statuses_timeline(screen_name)
    rescue Twitter::Error::NotFound
      errors.add(:screen_name, "User wasn't found.")
    end
  end

  private

  def trim
    if screen_name
      self.screen_name = screen_name[/\w*/]
    end
  end
end
