class Twitter::Model < SimpleFeedModel
  def provider
    @provider ||= Provider.twitter_for_user(user)
  end

  def client
    @client ||= ApiWrappers::Twitter.new(provider.token, provider.secret)
  end
end
