class Instagram::Model < SimpleFeedModel
  def set_provider
    self.provider ||= Provider.instagram_for_user(user)
  end

  def client
    @client ||= ApiWrappers::Instagram.new(provider.token)
  end
end
