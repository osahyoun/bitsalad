class Instagram::User < Instagram::Model
  attr_accessor :id, :handle
  validates :handle, presence: true
  #validate :user_exists

  RESOURCE = "users/media/recent"

  def meta
    { user_id: @user_id, handle: handle }
  end

  def user_exists
    users = client.user_search(handle)

    if users.any?
      user = users.first
      if user['username'] == handle
        @user_id = user['id']
      else
        user_not_found
      end
    else
      user_not_found
    end
  end

  def user_not_found
    errors[:handle] << "User wasn't found"
  end
end
