class Feed < ActiveRecord::Base
  belongs_to :user
  belongs_to :provider

  TOTAL_FEEDS_ALLOWED = 10
end
