class SimpleFeedModel
  extend ActiveModel::Callbacks
  extend ActiveModel::Naming
  include ActiveModel::Validations
  include ActiveModel::Conversion
  include ActiveModel::Model

  attr_accessor :user, :provider
  define_model_callbacks :save

  validates :provider, presence: true
  validate :feed_count

  before_save :set_provider

  def set_provider; end

  def save
    run_callbacks(:save){ true }
    valid?
  end

  def persisted?
   false
  end

  def feed_count
    if user.feeds.count >= ::Feed::TOTAL_FEEDS_ALLOWED
      errors.add(:base, "You've maxed out on feeds. Your limit is #{::Feed::TOTAL_FEEDS_ALLOWED}")
    end
  end

  class << self
    def create(opt)
      memo = new(opt)
      if memo.save
        Feed.create!(provider: memo.provider, resource: memo::class::RESOURCE, user: memo.user, meta: memo.meta)
      end
      memo
    end
  end
end
