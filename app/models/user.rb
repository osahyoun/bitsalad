class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable

  devise :database_authenticatable, :registerable, :confirmable,
    :recoverable, :rememberable, :trackable, :validatable


  has_many :providers
  has_many :feeds do

  end

  def provider_names
    @provider_names ||= providers.collect{|p| p.name}
  end

  def has_provider?(name)
    provider_names.include?(name)
  end

  def provider_for(name)
    Provider.send(name).for_user(self).first
  end

  def feeds_for(service)
    provider = providers.where(name: service).first
    provider ? provider.feeds.order('created_at desc') : []
  end
end
