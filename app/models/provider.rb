class Provider < ActiveRecord::Base

  scope :instagram, -> { where(name: "instagram") }
  scope :facebook,  -> { where(name: "facebook") }
  scope :twitter,   -> { where(name: "twitter") }
  scope :for_user,  -> (user) { where(user: user) }

  belongs_to :user
  has_many   :feeds, dependent: :destroy

  after_create :build_feed

  def build_feed
    if name.inquiry.instagram?
      Instagram::User.create(user: user, handle: nickname)
    end
  end

  class << self
    def instagram_for_user(user)
      instagram.find_by(user: user)
    end
    def facebook_for_user(user)
      facebook.find_by(user: user)
    end
    def twitter_for_user(user)
      twitter.find_by(user: user)
    end
  end
end
