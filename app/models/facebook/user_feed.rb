class Facebook::UserFeed < Facebook::Model
  attr_accessor :name
  before_save :set_name
  validate :check_unique

  RESOURCE = 'user/feed'

  def meta
    { name: self.name }
  end

  def valid?
    super
  end

  def set_name
    self.name = client.profile['name']
  end

  def check_unique
    if provider.feeds.where(resource: 'user/feed').exists?
      errors[:base] << "Feed for this user already exists"
    end
  end

  def client
    ::ApiWrappers::Facebook.new(provider.token)
  end
end

