class Facebook::Model < SimpleFeedModel


  def provider
    @provider ||= Provider.facebook_for_user(user)
  end
end
