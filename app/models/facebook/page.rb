class Facebook::Page < Facebook::Model
  attr_accessor :page

  validates :page, presence: true
  validate :page_exists

  RESOURCE = 'page'

  def meta
    { page: page, name: @name, url: @url }
  end

  def page_exists
    begin
      resp = client.get_page(page)
      @name = resp['name']
      @url = resp['link']

    rescue Koala::Facebook::ClientError
      errors[:page] << "This page might not exist."
    end
  end

  def client
    @client ||= Koala::Facebook::API.new(provider.token)
  end

end
