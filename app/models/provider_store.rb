class ProviderStore
  attr_reader :params, :user

  def initialize(user, params)
    @params = params
    @user = user
    puts params['info']
  end

  class << self
    def create(*args)
      new(*args).create
    end
  end

  def exists?
    provider
  end

  def provider
    @provider ||= user.providers.where(uid: params['uid'], name: params['provider']).first
  end

  def data
    { name:     params['provider'],
      uid:      params['uid'],
      token:    params['credentials']['token'],
      secret:   params['credentials']['secret'],
      nickname: params['info']['nickname'] }
  end

  def create
    unless exists?
      Provider.create!(data.merge(user_id: user.id))
    else
     provider.update_attributes! data
    end
  end
end
