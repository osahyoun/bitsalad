require "instagram"

Twitter::REST::Client.new do |config|
  config.consumer_key =     ENV['TWITTER_KEY']
  config.consumer_secret =  ENV['TWITTER_SECRET']
end

Instagram.configure do |config|
  config.client_id =      ENV['INSTAGRAM_KEY']
  config.client_secret =  ENV['INSTAGRAM_SECRET']
end

Rails.application.config.middleware.use OmniAuth::Builder do
  provider :twitter,    ENV['TWITTER_KEY'],   ENV['TWITTER_SECRET']
  provider :instagram,  ENV['INSTAGRAM_KEY'], ENV['INSTAGRAM_SECRET'], { scope: 'public_content'}
  provider(:facebook,   ENV['FACEBOOK_KEY'],  ENV['FACEBOOK_SECRET'],
    { scope: "offline_access, user_posts, user_photos, user_relationships, user_actions.news, user_activities, user_status, user_videos, user_website, user_about_me, user_birthday, user_hometown, user_location", ssl: { verify: false } }
  )
end
