require 'spec_helper'
require './lib/twitter_search_builder'

describe TwitterSearchBuilder do

  let(:params) do
    {
      all:   "one two",
      exact: "three four",
      any:   "five six",
      none:  "seven eight",
      hash:  "ten  #eleven"
    }

  end

  subject { TwitterSearchBuilder.new(params) }

  it "builds" do
    expect(subject.build).to eq(
      URI.encode(
        %{one two "three four" five OR six -seven -eight #ten #eleven}
      )
    )
  end

  it 'hashes' do
    expect(TwitterSearchBuilder.new({hash: " house   #dog"}).build).to(
      eq( URI.encode("#house #dog") )
    )
  end

  it 'any' do
    expect(TwitterSearchBuilder.new({any: " house  dog mouse"}).build).to(
      eq( URI.encode("house OR dog OR mouse") )
    )
  end

  it 'none' do
    expect(TwitterSearchBuilder.new({none: " house  dog mouse"}).build).to(
      eq( URI.encode("-house -dog -mouse") )
    )
  end

  it 'exact' do
    expect(TwitterSearchBuilder.new({exact: " house  dog mouse"}).build).to(
      eq( URI.encode(%{"house dog mouse"}) )
    )
  end

  it 'all' do
    expect(TwitterSearchBuilder.new({all: " house  dog mouse"}).build).to(
      eq( URI.encode("house dog mouse") )
    )
  end

end
