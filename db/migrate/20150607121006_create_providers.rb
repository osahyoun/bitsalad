class CreateProviders < ActiveRecord::Migration
  def change
    create_table :providers do |t|
      t.references :user, index: true, foreign_key: true
      t.string :name
      t.string :uid
      t.string :token
      t.string :secret
      t.string :nickname

      t.timestamps null: false
    end
  end
end
