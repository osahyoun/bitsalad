class CreateFeeds < ActiveRecord::Migration
  def change
    create_table :feeds do |t|
      t.string :resource
      t.datetime :fetched_at
      t.text :content
      t.json :meta
      t.references :user, index: true, foreign_key: true
      t.references :provider, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
